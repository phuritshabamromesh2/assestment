from django.urls import path
from .views import WeatherAPI

urlpatterns = [
    path('currentweather/', WeatherAPI.as_view(), name='weather'),
]